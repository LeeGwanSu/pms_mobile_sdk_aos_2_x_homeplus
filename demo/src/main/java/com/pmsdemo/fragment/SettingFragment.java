package com.pmsdemo.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.api.request.LoginPms;
import com.pms.sdk.api.request.SetConfig;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pmsdemo.R;

import org.json.JSONException;
import org.json.JSONObject;

public class SettingFragment extends BaseFragment implements View.OnClickListener
{
    EditText editTextAppKey;
    EditText editTextApiUrl;
    EditText editTextPrivateUrlTcp;
    EditText editTextPrivateUrlSsl;
    EditText editTextCustId;
    Button buttonSet;
    Button buttonMkt;
    Button buttonMsgNoti;
    Button buttonNtc;
    Button buttonLogin;
    Button buttonMqtt;
    Button buttonMsgFlag;
    TextView textViewMessage;
    private Prefs prefs;
    private boolean isMkt;
    private boolean isMsg;
    private boolean isNtc;
    private boolean isNoti;
    private boolean isMqtt;
    private boolean isProtocolTcp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.setting_fragment, null);
        editTextAppKey = view.findViewById(R.id.setting_edittext_appkey);
        editTextApiUrl = view.findViewById(R.id.setting_edittext_apiurl);
        editTextPrivateUrlTcp = view.findViewById(R.id.setting_edittext_privateurltcp);
        editTextPrivateUrlSsl = view.findViewById(R.id.setting_edittext_privateurlssl);
        editTextCustId = view.findViewById(R.id.setting_edittext_custid);
        buttonSet = view.findViewById(R.id.setting_button_set);
        buttonSet.setOnClickListener(this);
        buttonMkt = view.findViewById(R.id.setting_button_mkt);
        buttonMkt.setOnClickListener(this);
        buttonMsgNoti = view.findViewById(R.id.setting_button_msgnoti);
        buttonMsgNoti.setOnClickListener(this);
        buttonNtc = view.findViewById(R.id.setting_button_ntc);
        buttonNtc.setOnClickListener(this);
        buttonLogin = view.findViewById(R.id.setting_button_login);
        buttonLogin.setOnClickListener(this);
        buttonMqtt = view.findViewById(R.id.setting_button_mqtt);
        buttonMqtt.setOnClickListener(this);
        buttonMsgFlag = view.findViewById(R.id.setting_button_msgflag);
        buttonMsgFlag.setOnClickListener(this);
        textViewMessage = view.findViewById(R.id.setting_textview_message);
        editTextAppKey.setText(PMSUtil.getApplicationKey(getContext()));
        editTextApiUrl.setText(PMSUtil.getServerUrl(getContext()));
        editTextCustId.setText(PMSUtil.getCustId(getContext()));
        prefs = new Prefs(getContext());
        prefs.putString(PREF_MQTT_SERVER_CHECK,"Y");
        prefs.putString(PREF_API_SERVER_CHECK, "Y");
        prefs.putString(PREF_APP_KEY_CHECK, "Y");
        if(prefs.getString(IPMSConsts.PREF_MKT_FLAG).equals("Y"))
        {
            buttonMkt.setText("MKT(Y)");
            isMkt = true;
        }
        else
        {
            buttonMkt.setText("MKT(N)");
            isMkt = false;
        }
        if(prefs.getString(IPMSConsts.PREF_NTC_FLAG).equals("Y"))
        {
            buttonNtc.setText("NTC(Y)");
            isNtc = true;
        }
        else
        {
            buttonNtc.setText("NTC(N)");
            isNtc = false;
        }
        if(prefs.getString(PREF_MQTT_FLAG).equals("Y"))
        {
            buttonMqtt.setText("MQTT(Y)");
            isMqtt = true;
        }
        else
        {
            buttonMqtt.setText("MQTT(N)");
            isMqtt = false;
        }
        if(prefs.getString(IPMSConsts.PREF_MSG_FLAG).equals("Y"))
        {
            buttonMsgFlag.setText("MSG(Y)");
            isMsg = true;
        }
        else
        {
            buttonMsgFlag.setText("MSG(N)");
            isMsg = false;
        }
        return view;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.setting_button_set:
                PMS.getInstance(mCon).stopMQTTService(mCon);
                if(editTextAppKey.getText().toString().trim().length()>0)
                {
                    PMSUtil.setApplicationKey(getContext(), editTextAppKey.getText().toString().trim());
                    prefs.putString(IPMSConsts.PREF_APP_KEY, editTextAppKey.getText().toString().trim());
                }
                if(editTextApiUrl.getText().toString().trim().length()>0)
                {
                    PMSUtil.setServerUrl(getContext(), editTextApiUrl.getText().toString().trim());
                    prefs.putString(IPMSConsts.PREF_SERVER_URL, editTextApiUrl.getText().toString().trim());
                }
                if(editTextPrivateUrlSsl.getText().toString().trim().length()>0)
                {
                    PMSUtil.setMQTTServerUrl(getContext(), editTextPrivateUrlSsl.getText().toString().trim(),"");
                    prefs.putString(IPMSConsts.PREF_MQTT_SERVER_SSL_URL, editTextPrivateUrlSsl.getText().toString().trim());
                    prefs.putString(PREF_PRIVATE_PROTOCOL, "S");
                    buttonMsgFlag.setText("PROTOCOL(S)");
                    isProtocolTcp = false;
                }
                if(editTextPrivateUrlTcp.getText().toString().trim().length()>0)
                {
                    PMSUtil.setMQTTServerUrl(getContext(), "", editTextPrivateUrlTcp.getText().toString().trim());
                    prefs.putString(IPMSConsts.PREF_MQTT_SERVER_TCP_URL, editTextPrivateUrlTcp.getText().toString().trim());
                    prefs.putString(PREF_PRIVATE_PROTOCOL, "T");
                    buttonMsgFlag.setText("PROTOCOL(T)");
                    isProtocolTcp = true;
                }
                if(editTextPrivateUrlSsl.getText().toString().trim().length()>0 && editTextPrivateUrlTcp.getText().toString().trim().length()>0)
                {
                    PMSUtil.setMQTTServerUrl(getContext(), editTextPrivateUrlTcp.getText().toString().trim(), editTextPrivateUrlTcp.getText().toString().trim());
                    prefs.putString(IPMSConsts.PREF_MQTT_SERVER_SSL_URL, editTextPrivateUrlSsl.getText().toString().trim());
                    prefs.putString(IPMSConsts.PREF_MQTT_SERVER_TCP_URL, editTextPrivateUrlTcp.getText().toString().trim());
                    prefs.putString(PREF_PRIVATE_PROTOCOL, "T");
                    buttonMsgFlag.setText("PROTOCOL(T)");
                    isProtocolTcp = true;
                }
                if(editTextCustId.getText().toString().trim().length()>0)
                {
                    PMSUtil.setCustId(getContext(),editTextCustId.getText().toString().trim());
                    prefs.putString(IPMSConsts.PREF_APP_KEY, editTextAppKey.getText().toString().trim());
                }
                textViewMessage.setText("DeviceCert 진행중");

                new DeviceCert(getContext()).request(null, new APIManager.APICallback() {
                    @Override
                    public void response(String code, JSONObject json)
                    {
                        JSONObject parameter = getUserDataJson();
                        if(parameter!=null)
                        {
                            textViewMessage.setText("code : "+code+"\njson : "+json.toString()+"\nparameter : "+parameter.toString());
                        }
                        else
                        {
                            textViewMessage.setText("code : "+code+"\njson : "+json.toString());
                        }
                    }
                });
                PMS.getInstance(mCon).startMQTTService(mCon);

                break;
            case R.id.setting_button_mkt:
                if(isMkt)
                {
                    prefs.putString(IPMSConsts.PREF_MKT_FLAG, "N");
                    textViewMessage.setText("설정완료");
                    buttonMkt.setText("MKT(N)");
                    isMkt = false;
                }
                else
                {
                    prefs.putString(IPMSConsts.PREF_MKT_FLAG, "Y");
                    textViewMessage.setText("설정완료");
                    buttonMkt.setText("MKT(Y)");
                    isMkt = true;
                }
                break;
            case R.id.setting_button_mqtt:
                if(isMqtt)
                {
                    prefs.putString(IPMSConsts.PREF_MQTT_FLAG, "N");
                    PMSUtil.setMQTTFlag(mCon, false);
                    isMqtt = false;
                    textViewMessage.setText("설정완료");
                    buttonMqtt.setText("MQTT(N)");
                    isMqtt = false;
                }
                else
                {
                    prefs.putString(IPMSConsts.PREF_PRIVATE_FLAG, "Y");
                    PMSUtil.setMQTTFlag(mCon, true);
                    isMqtt = true;
                    textViewMessage.setText("설정완료");
                    buttonMqtt.setText("MQTT(Y)");
                    isMqtt = true;
                }
                break;
            case R.id.setting_button_ntc:
                if(isNtc)
                {
                    prefs.putString(IPMSConsts.PREF_NTC_FLAG, "N");
                    textViewMessage.setText("설정완료");
                    buttonNtc.setText("NTC(N)");
                    isNtc = false;
                }
                else
                {
                    prefs.putString(IPMSConsts.PREF_NTC_FLAG, "Y");
                    textViewMessage.setText("설정완료");
                    buttonNtc.setText("NTC(Y)");
                    isNtc = true;
                }
                break;
            case R.id.setting_button_login:
                textViewMessage.setText("진행중");
                new LoginPms(mCon).request(editTextCustId.getText().toString().trim(), getUserDataJson(), new APIManager.APICallback() {
                    @Override
                    public void response(String code, JSONObject json)
                    {
                        JSONObject parameter = getUserDataJson();
                        if(parameter!=null)
                        {
                            textViewMessage.setText("code : "+code+"\njson : "+json.toString()+"\nparameter : "+parameter.toString());
                        }
                        else
                        {
                            textViewMessage.setText("code : "+code+"\njson : "+json.toString());
                        }
                    }
                });
                break;
            case R.id.setting_button_msgnoti:
                textViewMessage.setText("진행중");
                new SetConfig(mCon).request(mPref.getString(IPMSConsts.PREF_MSG_FLAG), mPref.getString(IPMSConsts.PREF_NOTI_FLAG), new APIManager.APICallback() {
                    @Override
                    public void response(String code, JSONObject json)
                    {
                        textViewMessage.setText("code : "+code+"\njson : "+json.toString());
                    }
                });
                break;
            case R.id.setting_button_msgflag:
                if(isMsg)
                {
                    prefs.putString(IPMSConsts.PREF_MSG_FLAG, "N");
                    textViewMessage.setText("설정완료");
                    buttonMsgFlag.setText("MSG(N)");
                    isMsg = false;
                }
                else
                {
                    prefs.putString(IPMSConsts.PREF_MSG_FLAG, "Y");
                    textViewMessage.setText("설정완료");
                    buttonMsgFlag.setText("MSG(Y)");
                    isMsg = true;
                }
                break;
        }
    }
    private JSONObject getUserDataJson()
    {
        JSONObject jobj = null;
        try {
            jobj = new JSONObject();

            // new version
            jobj.put("appKey", PMSUtil.getApplicationKey(getContext()));
            jobj.put("uuid", PMSUtil.getUUID(getContext()));
//			jobj.put("pushToken", PMSUtil.getGCMToken(mContext));
            String strToken = PMSUtil.getGCMToken(getContext());
            if (StringUtil.isEmpty(strToken) || NO_TOKEN.equals(strToken)) {
//                strToken = GetFCMInstanceID.getToken();
                if (StringUtil.isEmpty(strToken)) {
                    strToken = NO_TOKEN;
                }
                PMSUtil.setGCMToken(getContext(), strToken);
            }
            jobj.put("pushToken", strToken);
            jobj.put("custId", PMSUtil.getCustId(getContext()));
            jobj.put("appVer", PhoneState.getAppVersion(getContext()));
            jobj.put("os", "A");
            jobj.put("osVer", PhoneState.getOsVersion());
            jobj.put("device", PhoneState.getDeviceName());
            jobj.put("sessCnt", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jobj;
    }
}
