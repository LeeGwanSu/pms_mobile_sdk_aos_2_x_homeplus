package com.pms.sdk;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.*;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.FCMRequestToken;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.Badge;

import java.io.Serializable;
import java.util.Stack;

/**
 * @author erzisk
 * @description pms (solution main class) homeplus
 * @version [2014.01.13 09:31] MQTT Client 안정화 & Connect Check 부분 수정함. <br>
 *          [2014.02.07 17:12] Collect 부분 수정함. <br>
 *          [2014.02.11 10:18] CollectLog getParam 부분 수정함. <br>
 *          [2014.02.21 17:23] MQTT Client 안정화 및 Prvate 서버 Protocol 적용 가능하게 수정함.<br>
 *          [2014.02.25 10:12] 안쓰는 method 삭제함.<br>
 *          [2014.03.03 10:36] 팝업 셋팅 default 설정 가능하게 변경.<br>
 *          [2014.03.05 15:31] MQTT Client 관련 Service 추가.<br>
 *          [2014.03.05 20:37] MQTT Client STOP 재시작하는 현상 수정.<br>
 *          [2014.03.13 10:47] MQTT Client keepAlive 수정함. sleep mode 삭제.<br>
 *          [2014.03.28 09:29] DisusePms.m API 추가함.<br>
 *          [2014.05.14 20:06] DisusePms.m API 삭제 & Popup 설정값 추가.<br>
 *          [2014.06.02 20:39] Popup WebView background 수정함. <br>
 *          [2014.06.05 18:24] 다른앱 실행시 팝업창 미 노출. <br>
 *          [2014.06.09 11:05] 팝업창에 대한 Flag 값 추가함. <br>
 *          [2014.06.11 17:32] 텍스트 푸쉬일때 toast 메세지로 전환할수 있도록 수정. <br>
 *          [2014.06.16 20:44] 다른앱 사용시 안뜨는 플래그 추가함. <br>
 *          [2014.06.24 09:10] ReadMSg UserMsgId로 가능하게 수정함. <br>
 *          [2014.07.09 16:26] Notification Bar 사용자가 만들수 있도록 수정함. <br>
 *          [2014.07.14 10:27] MQTT 안정화 <br>
 *          [2014.08.07 15:05] MQTT 안정화 & GCM 분기처리함. <br>
 *          [2014.08.22 10:06] 팝업 허용시간 추가 및 Notification 색상 적용함. <br>
 *          [2014.08.26 16:05] 팝업 허용시간 설정하는 Flag값추가, setMsgKind.m API 추가함. <br>
 *          [2014.08.28 17:13] getMsgKind.m API 추가함. <br>
 *          [2014.09.03 14:36] newMsg Count에 Recovery Flag가 'N'인 것만 Count되게 함. <br>
 *          [2014.09.05 13:41] 무음 모드일경우 밸소리 & 진동 울리지 않음. <br>
 *          [2014.09.11 10:08] 시스템 소리 설정에 따라 동작하도록 수정함. <br>
 *          [2014.09.12 14:05] No Cache 적용, Icon Meta tag적용, 팝업 플래그 기존거 추가. <br>
 *          [2014.10.21 14:33] Android OS 5.0에서 상태창 이미지 표시 안되는 버그 수정함. <br>
 *          [2015.01.19 16:04] Rich Popup에서 이미지 안뜨는 현상 수정함. <br>
 *          [2015.01.21 10:26] 소스취약점보고서에서 문제점 수정함. (printStackTrace() -> getMessage()로 교체)<br>
 *          [2015.03.13 16:08] 구글 정책에 따른 앱 실행중일떄만 팝업창 노출.<br>
 *          [2016.04.29 09:58] X509TrustManager 코드 삭제하고 대응 코드 삽입함.<br>
 *          [2016.05.17 09:32] 정통법 사항 추가함.<br>
 *          [2016.05.30 10:31] getFlag API추가함.<br>
 *          [2016.07.04 11:29] mktVer 플래그 추가함.<br>
 *          [2016.11.29 09:45] doze mode broadcast 패치1 적용 <br>
 *          [2016.12.14 13:21] Icon에 대한 버그 사항 수정함.<br>
 *          [2017.01.31 10:00] Notification Big Text 사용하도록 수정함.<br>
 *          [2017.02.20 14:11] 2016.07.05 작업에 대한 내용 원복(mktVer 플래그 추가함에따라 DeviceCert수정된 내용)<br>
 *          [2017.03.02 15:37] Lollipop 이상 Notification small icon color지정기능 추가 <br>
 *          [2017.03.08 11:29] Nougat 이상 Notification group기능 추가 <br>
 *          [2017.03.24 14:18] mktFlag에서도 notification이 뜨게 수정, Notification bug fix <br>
 *          [2017.03.28 10:02] notification back color bugfix <br>
 *          [2017.06.15 11:42] Notification 캐리지리턴 기능추가 <br>
 *          [2017.09.26 11:42] DB로 UUID 관리하도록 수정 <br>
 *          [2017.10.27 10:57] * Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *          					* DB 중복 Open으로 인해 강제종료 문제 수정
 *          					* PushPopup 발생 시 강제 종료 문제 수정
 *          					* UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 *          					  UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함 <br>
 *          [2018.06.18 10:49] 8.0 대응, FCM 대응 <br>
 *          [2018.07.05 09:56] JobScheduler 대응, Android 8.0 NotiSound 버그 수정 <br>
 *          [2018.07.05 15:53] MQTTJobService 오류 수정
 *          - INTENT_RECEIVED_SESSION_TIME_OUT 이 com.tms.sdk.push.mqtt.polling로 나감
 *          - SSL SignKey, SignPass 못가져오게 되어있음(TMS는 DB에서 가져오는데 PMS에는 PREF)
 *          - PMSUtil getMQTTPollingFlag, getMQTTPollingInterval Setter 없고, Properties에서 가져오기로 되어있었음
 *         [2018.07.12 11:30] 8.0기기에서 NotificationChannel Sound,Badge,Vibe 안나던 이슈 수정
 *         [2018.07.26 09:38] 앱 강제 종료시 JobScheduler도 같이 종료되는 현상 조치하기 전까지 무조건 MQTTService 사용하도록 설정
 *         [2018.07.26 13:30] 암시적 Intent 오류 수정
 *         [2018.08.31 13:44] MQTT 간소화 버전 교체, WRITE_EXTERNAL_STORAGE 제거, 이미지 캐시 에러 대응, NotificationChannel 진동 대응
 *         [2018.09.10 18:18] MQTT 간소화 버그 수정
 *         [2018.09.11 13:50] setNotiColor 추가
 *         [2018.09.19 24:05] MQTT 버그 대응
 *         [2018.09.21 09:10] 푸시 못받는 버그 수정, MqttWakeTime 적용
 *         [2018.09.21 10:39] MQTT 오류 대응
 *         [2018.10.19 14:42] MQTT 안정화
 *         [2018.10.23 09:54] MQTT 수정
 *         [2019.01.08 15:16] MQTTStarter 삭제, 분기별 서비스 실행 적용
 *         [2019.02.01 15:43] FCMDeprecated 대응, DB 커서 Null 예외처리
 *         [2019.02.01 16:17] MQTT Binder 최신사항 반영
 *         [2019.03.20 15:29] 벨소리 볼륨 로직 제거
 *         [2019.04.17 09:07] FCM 토큰 발급 개선
 *         [2019.05.20 10:01] Private Service 안정화 로직 적용
 *         [2019.09.05 11:06] TLS 1.2 대응, 업체 요청으로 알림채널 강제 재생성 로직 제거, ReadMsg kk->HH 대응, 잠금화면 체크 추가
 *         [2020.03.30 10:43] 홈플러스 푸시 토큰이 갱신되지 않는다고 해서 DeviceCert API 호출할때마다 토큰 갱신하도록함
 *
 *
 */
public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;
	private static PMS instancePms;
	private static PMSPopup instancePmsPopup;
	private Context mContext;

	private final PMSDB mDB;
	private final Prefs mPrefs;

	private OnReceivePushListener onReceivePushListener;

	/**
	 * constructor
	 * 
	 * @param context
	 */
	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		this.mPrefs = new Prefs(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:202003301043");

        initOption(context);
    }

    /**
     * getInstance
     *
     * @param context
     * @param projectId
     * @return
     */
    public static PMS getInstance(Context context, String projectId) {
        CLog.d("getInstance projectId : " + projectId);
        PMSUtil.setGCMProjectId(context, projectId);
        return getInstance(context);
    }

    /**
     * getInstance
     *
     * @param context
     * @return
     */
    public static PMS getInstance(final Context context) {
        CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
        if (instancePms == null) {
            instancePms = new PMS(context);
//            if (PhoneState.isAvailablePush()) {
//                PushReceiver.gcmRegister(context, PMSUtil.getGCMProjectId(context));
//            }
        }
        instancePms.setmContext(context);
        String token = PMSUtil.getGCMToken(context);
        if (TextUtils.isEmpty(token) || NO_TOKEN.equals(token))
        {
            // get token
            new FCMRequestToken(context, PMSUtil.getGCMProjectId(context), new FCMRequestToken.Callback() {
                @Override
                public void callback(boolean isSuccess, String message)
                {
                    CLog.i("FCMRequestToken "+isSuccess+" / "+message);
                }
            }).execute();
        }
        return instancePms;
    }

    public static PMSPopup getPopUpInstance() {
        return instancePmsPopup;
    }

    /**
     * clear
     *
     * @return
     */
    public static boolean clear() {
        try {
            PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
            instancePms.unregisterReceiver();
            instancePms = null;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * initialize option
     */
    private void initOption(Context context) {
        if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
            mPrefs.putString(PREF_NOTI_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
            mPrefs.putString(PREF_MSG_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
            mPrefs.putString(PREF_RING_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
            mPrefs.putString(PREF_VIBE_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
            mPrefs.putString(PREF_ALERT_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
            mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
            mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
        }
        if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
            PMSUtil.setServerUrl(context, API_SERVER_URL);
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
            mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
        }
        if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
            mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
        }
    }

    private void setmContext(Context context) {
        this.mContext = context;
    }

    private void unregisterReceiver() {
        Badge.getInstance(mContext).unregisterReceiver();
    }

    public void setPopupSetting(Boolean state, String title) {
        instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
    }

    /**
     * get cust id
     *
     * @return
     */
    public String getCustId() {
        CLog.i("getCustId");
        return PMSUtil.getCustId(mContext);
    }

    /**
     * cust id 세팅
     *
     * @param custId
     */
    public void setCustId(String custId) {
        CLog.i("setCustId");
        CLog.d("setCustId:custId=" + custId);
        PMSUtil.setCustId(mContext, custId);
    }

    public void setIsPopupActivity(Boolean ispopup) {
        PMSUtil.setPopupActivity(mContext, ispopup);
    }

    public boolean getIsPopupActivity() {
        return PMSUtil.getPopupActivity(mContext);
    }

    /**
     * set debugTag
     *
     * @param tagName
     */
    public void setDebugTAG(String tagName) {
        CLog.setTagName(tagName);
    }

    /**
     * set debug mode
     *
     * @param debugMode
     */
    public void setDebugMode(boolean debugMode) {
        CLog.setDebugMode(debugMode);
    }

    public OnReceivePushListener getOnReceivePushListener() {
        return onReceivePushListener;
    }

    public void setOnReceivePushListener(OnReceivePushListener onReceivePushListener) {
        this.onReceivePushListener = onReceivePushListener;
    }

    /**
     * get msg flag
     *
     * @return
     */
    public String getMsgFlag() {
        return mPrefs.getString(PREF_MSG_FLAG);
    }

    /**
     * get noti flag
     *
     * @return
     */
    public String getNotiFlag() {
        return mPrefs.getString(PREF_NOTI_FLAG);
    }

    public String getMktFlag() {
        return mPrefs.getString(IPMSConsts.PREF_MKT_FLAG);
    }

    public String getntcFlag() {
        return mPrefs.getString(IPMSConsts.PREF_NTC_FLAG);
    }

    /**
     * set noti icon
     *
     * @param resId
     */
    public void setNotiIcon(int resId) {
        mPrefs.putInt(PREF_NOTI_ICON, resId);
    }

    /**
     * set large noti icon
     *
     * @param resId
     */
    public void setLargeNotiIcon(int resId) {
        mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
    }

    /**
     * set noti cound
     *
     * @param resId
     */
    public void setNotiSound(int resId) {
        mPrefs.putInt(PREF_NOTI_SOUND, resId);
    }

    /**
     * set noti receiver
     *
     * @param intentAction
     */
    public void setNotiReceiver(String intentAction) {
        mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
    }

    /**
     * set noti receiver class (Class name)
     * @param intentAction
     */
    public void setNotiReceiverClass (String intentAction) {
        mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, intentAction);
    }
    /**
     * set noti receiver (Intent action)
     * @param intentAction
     */
    public void setPushReceiverClass (String intentAction) {
        mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, intentAction);
    }

    /**
     * set ring mode
     *
     * @param isRingMode
     */
    public void setRingMode(boolean isRingMode) {
        mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
    }

    /**
     * get ring mode
     */
    public String getRingMode() {
        String strRet = "";
        strRet = mPrefs.getString(PREF_RING_FLAG);
        return strRet;
    }

    /**
     * set vibe mode
     *
     * @param isVibeMode
     */
    public void setVibeMode(boolean isVibeMode) {
        mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
    }

    /**
     * get vibe mode
     */
    public String getVibeMode() {
        String strRet = "";
        strRet = mPrefs.getString(PREF_VIBE_FLAG);
        return strRet;
    }

    /**
     * set popup noti
     *
     * @param isShowPopup
     */
    public void setPopupNoti(boolean isShowPopup) {
        mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
    }

    /**
     * get PopupNoti mode
     */
    public String getPopupNoti() {
        String strRet = "";
        strRet = mPrefs.getString(PREF_ALERT_FLAG);
        return strRet;
    }

    /**
     * set screen wakeup
     *
     * @param isScreenWakeup
     */
    public void setScreenWakeup(boolean isScreenWakeup) {
        mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
    }

    /**
     * set push popup activity
     *
     * @param classPath
     */
    public void setPushPopupActivity(String classPath) {
        mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
    }

    public void setUseBigText(Boolean state) {
        mPrefs.putString(PREF_USE_BIGTEXT, state ? "Y" : "N");
    }


    /**
     * set pushPopup showing time
     *
     * @param pushPopupShowingTime
     */
    public void setPushPopupShowingTime(int pushPopupShowingTime) {
        mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
    }

    /**
     * set inbox activity
     *
     * @param inboxActivity
     */
    public void setInboxActivity(String inboxActivity) {
        mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
    }

    public void bindBadge(Context c, int id) {
        Badge.getInstance(c).addBadge(c, id);
    }

    public void bindBadge(TextView badge) {
        Badge.getInstance(badge.getContext()).addBadge(badge);
    }

    /**
     * show Message Box
     *
     * @param c
     */
    public void showMsgBox(Context c) {
        showMsgBox(c, null);
    }

    public void showMsgBox(Context c, Bundle extras) {
        CLog.i("showMsgBox");
        if (PhoneState.isAvailablePush()) {
            try {
                Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

                Intent i = new Intent(mContext, inboxActivity);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (extras != null) {
                    i.putExtras(extras);
                }
                c.startActivity(i);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            // Intent i = new Intent(INBOX_ACTIVITY);
            // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // if (extras != null) {
            // i.putExtras(extras);
            // }
            // c.startActivity(i);
        }
    }

    public void closeMsgBox(Context c) {
        Intent i = new Intent(c, Badge.class).setAction(RECEIVER_CLOSE_INBOX);
        c.sendBroadcast(i);
    }

    /**
     * start mqtt service
     */
    public void startMQTTService(Context context) {
        if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context))) {
            context.sendBroadcast(new Intent(context, RestartReceiver.class).setAction(ACTION_START));
        } else {
            context.stopService(new Intent(context, MQTTService.class));
        }
    }

    public void stopMQTTService(Context context) {
        context.stopService(new Intent(context, MQTTService.class));
    }

    /**
     * set ServerUrl
     *
     * @param serverUrl
     */
    public void setServerUrl(String serverUrl) {
        CLog.i("setServerUrl");
        CLog.d("setServerUrl:serverUrl=" + serverUrl);
        PMSUtil.setServerUrl(mContext, serverUrl);
    }

	/*
     * ===================================================== [start] database =====================================================
	 */

    /**
     * select MsgGrp list
     *
     * @return
     */
    public Cursor selectMsgGrpList() {
        return mDB.selectMsgGrpList();
    }

    /**
     * select MsgGrp
     *
     * @param msgCode
     * @return
     */
    public MsgGrp selectMsGrp(String msgCode) {
        return mDB.selectMsgGrp(msgCode);
    }

    /**
     * select new msg Cnt
     *
     * @return
     */
    public int selectNewMsgCnt() {
        return mDB.selectNewMsgCnt();
    }

    /**
     * select Msg List
     *
     * @param page
     * @param row
     * @return
     */
    public Cursor selectMsgList(int page, int row) {
        return mDB.selectMsgList(page, row);
    }

    /**
     * select Msg List
     *
     * @param msgCode
     * @return
     */
    public Cursor selectMsgList(String msgCode) {
        return mDB.selectMsgList(msgCode);
    }

    /**
     * select Msg
     *
     * @param msgId
     * @return
     */
    public Msg selectMsgWhereMsgId(String msgId) {
        return mDB.selectMsgWhereMsgId(msgId);
    }

    /**
     * select Msg
     *
     * @param userMsgID
     * @return
     */
    public Msg selectMsgWhereUserMsgId(String userMsgID) {
        return mDB.selectMsgWhereUserMsgId(userMsgID);
    }

    /**
     * select query
     *
     * @param sql
     * @return
     */
    public Cursor selectQuery(String sql) {
        return mDB.selectQuery(sql);
    }

    /**
     * update MsgGrp
     *
     * @param msgCode
     * @param values
     * @return
     */
    public long updateMsgGrp(String msgCode, ContentValues values) {
        return mDB.updateMsgGrp(msgCode, values);
    }

    /**
     * update read msg
     *
     * @param msgGrpCd
     * @param firstUserMsgId
     * @param lastUserMsgId
     * @return
     */
    public long updateReadMsg(String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
        return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
    }

    /**
     * update read msg where userMsgId
     *
     * @param userMsgId
     * @return
     */
    public long updateReadMsgWhereUserMsgId(String userMsgId) {
        return mDB.updateReadMsgWhereUserMsgId(userMsgId);
    }

    /**
     * update read msg where msgId
     *
     * @param msgId
     * @return
     */
    public long updateReadMsgWhereMsgId(String msgId) {
        return mDB.updateReadMsgWhereMsgId(msgId);
    }

    /**
     * delete Msg
     *
     * @param userMsgId
     * @return
     */
    public long deleteUserMsgId(String userMsgId) {
        return mDB.deleteUserMsgId(userMsgId);
    }

    /**
     * delete Msg
     *
     * @param MsgId
     * @return
     */
    public long deleteMsgId(String MsgId) {
        return mDB.deleteMsgId(MsgId);
    }

    /**
     * delete MsgGrp
     *
     * @param msgCode
     * @return
     */
    public long deleteMsgGrp(String msgCode) {
        return mDB.deleteMsgGrp(msgCode);
    }

    /**
     * delete expire Msg
     *
     * @return
     */
    public long deleteExpireMsg() {
        return mDB.deleteExpireMsg();
    }

    /**
     * delete empty MsgGrp
     *
     * @return
     */
    public void deleteEmptyMsgGrp() {
        mDB.deleteEmptyMsgGrp();
    }

    /**
     * delete all
     */
    public void deleteAll() {
        mDB.deleteAll();
    }

	/*
	 * ===================================================== [end] database =====================================================
	 */

    public String getPushToken () {
        return PMSUtil.getGCMToken(mContext);
    }

    private static Stack<Intent> serviceStack = new Stack<>();

    public static Stack<Intent> getServiceStack()
    {
        return serviceStack;
    }

    public static void setServiceStack(Intent serviceId)
    {
        serviceStack.push(serviceId);
    }
}
