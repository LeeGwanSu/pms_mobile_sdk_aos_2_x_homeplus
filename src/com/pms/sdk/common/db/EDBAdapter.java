package com.pms.sdk.common.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author erzisk
 * @since 2013.01.02
 * @class EDBAdapter
 * @description erzisk db adapter (sqlite)
 */
abstract public class EDBAdapter {

	protected static String DATABASE_NAME = "default.db";
	protected static int DATABASE_VERSION = 1;

	private Context mContext;

	private SQLiteDatabase mDB;
	private EDBOpenHelper mHelper;

	protected EDBAdapter(Context c) {
		this.mContext = c;
	}

	private void open (Context c) {
		if (this.mDB == null || !this.mDB.isOpen()) {
			this.mHelper = new EDBOpenHelper(c, DATABASE_NAME, DATABASE_VERSION);
			this.mDB = this.mHelper.getWritableDatabase();
		}
	}

	private void close () {
		// if (this.mDB != null && this.mDB.isOpen()) {
		// this.mDB.close();
		// this.mHelper.close();
		// }
	}

	protected long insert (String table, String nullColumnHack, ContentValues values) {
		try {
			this.open(mContext);
			return this.mDB.insert(table, nullColumnHack, values);
		} catch (Exception e) {
			this.printError(e);
			return -1;
		} finally {
			this.close();
		}

	}

	protected long update (String table, ContentValues values, String whereClause, String[] whereArgs) {
		try {
			this.open(mContext);
			return this.mDB.update(table, values, whereClause, whereArgs);
		} catch (Exception e) {
			this.printError(e);
			return -1;
		} finally {
			this.close();
		}
	}

	protected long delete (String table, String whereClause, String[] whereArgs) {
		try {
			this.open(mContext);
			return this.mDB.delete(table, whereClause, whereArgs);
		} catch (Exception e) {
			this.printError(e);
			return -1;
		} finally {
			this.close();
		}
	}

	protected void execSQL (String sql) {
		try {
			this.open(mContext);
			this.mDB.execSQL(sql);
		} catch (Exception e) {
			this.printError(e);
		} finally {
			this.close();
		}
	}

	protected void execSQL (String sql, Object[] bindArgs) {
		try {
			this.open(mContext);
			this.mDB.execSQL(sql, bindArgs);
		} catch (Exception e) {
			this.printError(e);
		} finally {
			this.close();
		}
	}

	protected Cursor rawQuery (String sql, String[] selectionArgs) {
		try {
			this.open(mContext);
			return this.mDB.rawQuery(sql, selectionArgs);
		} catch (Exception e) {
			this.printError(e);
			return null;
		} finally {
			this.close();
		}
	}

	protected Cursor query (String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
		try {
			this.open(mContext);
			return this.mDB.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
		} catch (Exception e) {
			this.printError(e);
			return null;
		} finally {
			this.close();
		}
	}

	abstract protected void printError (Exception e);

	abstract protected void onDBCreate (SQLiteDatabase db);

	abstract protected void onDBUpgrade (SQLiteDatabase db, int oldVersion, int newVersion);

	public class EDBOpenHelper extends SQLiteOpenHelper {

		public EDBOpenHelper(Context context, String name, int version) {
			super(context, name, null, version);
		}

		public EDBOpenHelper(Context context, String name, CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate (SQLiteDatabase db) {
			onDBCreate(db);
		}

		@Override
		public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion) {
			onDBUpgrade(db, oldVersion, newVersion);
		}

	}
}
