package com.pms.sdk.common.util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @since 2012.01.14
 * @author erzisk
 * @description string util
 */
public class StringUtil {

	/**
	 * is empty arr (or)
	 * 
	 * @param arrStr
	 * @return true : array이 안의 string중 하나라도 "" or null 이면 true
	 */
	public static boolean isEmptyArr (String[] arrStr) {
		for (String s : arrStr) {
			if (isEmpty(s)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * is empty
	 * 
	 * @param str
	 * @return true : "" or null
	 */
	public static boolean isEmpty (String str) {
		if (str == null || "".equals(str)) {
			return true;
		}
		return false;
	}

	/**
	 * is contain (or)
	 * 
	 * @param str
	 * @param target
	 * @return true : target 안의 string중 하나라도 str과 같으면 true
	 */
	public static boolean isContain (String str, String[] target) {
		for (String s : target) {
			if (s.equals(str)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Dec To Hex
	 * 
	 * @param dec
	 *        integer
	 * @return String Hex
	 */
	public static String intToHex (int dec) {
		if ((dec >= 0) && (dec < 16)) {
			return "0" + Integer.toHexString(dec);
		} else {
			return Integer.toHexString(dec);
		}
	}

	/**
	 * @author haewon
	 */
	public static String toJson(Map<?, Object> map) throws JSONException
	{
		JSONObject obj = new JSONObject();

		Iterator keys = map.keySet().iterator();
		while (keys.hasNext())
		{
			String key = keys.next().toString();
			obj.put(key, map.get(key));
		}

		return obj.toString();
	}

	public static HashMap<String, Object> fromJson(JSONObject obj)
	{
		HashMap<String, Object> clz = new HashMap<>();

		Iterator keys = obj.keys();
		while (keys.hasNext())
		{
			String key = keys.next().toString();
			clz.put(key, obj.opt(key));
		}

		return clz;
	}
}
